package com.company;

public class Calculations {

    public static Point2D positionGeometricCenter(Point2D[] points) {
       double tempX = 0.0, tempY = 0.0;

       for(int i = 0; i < points.length; i++) {
           tempX = tempX + points[i].getX();
           tempY = tempY + points[i].getY();
       }
       return new Point2D(tempX / points.length, tempY / points.length);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoints) {
        double tempX = 0.0, tempY = 0.0, tempMass = 0.0;

        for(MaterialPoint2D materialPoint2D: materialPoints) {
            tempX += materialPoint2D.getX() * materialPoint2D.getMass();
            tempY += materialPoint2D.getY() * materialPoint2D.getMass();
            tempMass += materialPoint2D.getMass();
        }

        return new MaterialPoint2D(tempX / tempMass,tempY / tempMass, tempMass);
    }
}
