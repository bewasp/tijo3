package com.company;

public class MaterialPoint2D extends Point2D{

    private double x, y, mass;

    public MaterialPoint2D(double x, double y, double mass) {
        super(x, y);
        this.mass = mass;
        this.x = x;
        this.y = y;
    }

    public double getMass() {
        return mass;
    }

    public String toString() {
        return getX() + " " + getY() + " " + getMass();
    }
}
